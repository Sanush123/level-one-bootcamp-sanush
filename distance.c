#include<stdio.h>
#include<math.h>
int get1(){
    int x;
    printf("enter the value of x1 coordinate of 1st point:");
    scanf("%d",&x);
    return x;
}
int get2(){
    int x;
    printf("\nenter the value of y1 coordinate of 1st point:");
    scanf("%d",&x);
    return x;
}
int get3(){
    int x;
    printf("\nenter the value of x2 coordinate of 2nd point:");
    scanf("%d",&x);
    return x;
}
int get4(){
    int x;
    printf("\nenter the value of y2 coordinate of 2nd point:");
    scanf("%d",&x);
    return x;
}
int main(){
    int x1,y1,x2,y2;
    float dist;
    x1=get1();
    y1=get2();
    x2=get3();
    y2=get4();
    
    dist=((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    printf("\n The distance between two points is:%f",sqrt(dist));
    return 0;
}